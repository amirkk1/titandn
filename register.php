<?php
include("config.php");


if(isset($_POST["submit"])){
  if(isset($_POST['regusername']) && isset($_POST['regpassword']) && isset($_POST['regcpassword']) && isset($_POST['regemail']))
  {
    
    $user=$_POST['regusername'];
    $pass=$_POST['regpassword'];
    $rpass=$_POST['regcpassword'];
    $email=$_POST['regemail'];

    
    $SQL = sqlConnect();
    //$SQL->select('dnmembership');
    
    if(checkToken($_POST['t_M6V4SVaH']) != 1)
    {
       $error .= "<center><font color='white'>Invalid Registration Token</font></center>";
    }

    if($SQL->query("select * from dnmembership.dbo.Accounts where AccountName='$user'")){
       if($SQL->numRows() > 0){
         $error .= "<center><button class='btn btn-warning btn-s'>Name already exists , choose another one!</button></center><br />";
       }
    }

    if($SQL->query("select * from dnmembership.dbo.Accounts where Email='$email'")){
       if($SQL->numRows() > 0){
         $error .= "<center><button class='btn btn-warning btn-s'>E-mail address already exists , choose another one!</button></center><br />";
       }
    }
    
    if(preg_match('/[^a-z_\-0-9]/i', $user))
    {
       $error .= "<center><button class='btn btn-warning btn-s'>Use only english letters and numbers!</button></center><br />";
    }

    if(strlen($user) < 6 || strlen($user) > 18)
    {
      $error .= "<center><button class='btn btn-warning btn-s'>The username is too long or too short!</button></center><br />";
    }

    if($pass != $rpass)
    {
      $error .= "<center><button class='btn btn-warning btn-s'>Passwords does not match!</button></center><br />";
    }

    if(strlen($pass) < 6 || strlen($pass) > 18 || strlen($rpass) < 6 || strlen($rpass) > 18)
    {
      $error .= "<center><button class='btn btn-warning btn-s'>The password is too long or too short!</button></center><br />";
    }
      
    if(!isset($error))
    {
         $SQL->query("insert into dnmembership.dbo.Accounts (AccountName,AccountLevelCode,SecondAuthFailCount,SecondAuthCode,SecondAuthLockFlag,CharacterCreateLimit,CharacterMaxCount,RegisterDate,PublisherCode,NxLoginPwd,Email) values ('$user',0,0,1,'false',4,8,GETDATE(),0,'".strtoupper(md5($pass))."','$email')");
         $success = "<center>Account <b>$user</b> has been successfully created !<br /><br /></center>";
    }

  }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>TitansDN - Register</title>
<link href="themes/v7/images/favicon.gif" rel="shortcut icon"/>
<link href="themes/v7/images/favicon.gif" rel="icon" type="image/gif"/>
<link href="themes/v7/style.css" type="text/css" rel="stylesheet"/>
<link href="themes/v7/grades.css" type="text/css" rel="stylesheet"/>
<link href="themes/v7/jquery.ambiance.css" type="text/css" rel="stylesheet"/>
<link href="themes/v7/nivo-slider/theme.css" type="text/css" media="screen" rel="stylesheet"/>

</head>
<body>
<div id="trailer-overlay">
<div id="trailer-cont">
<div id="trailer-yt"></div>
<span id="trailer-share"><a href="#" target="_blank" class="facebook" title="Share on Facebook"></a><a href="#" target="_blank" class="twitter" title="Tweet it out!"></a></span>
</div>
</div>
<div id="banner2">
<div class="msg">No Hacking</div>
<div class="msg">No Inappropriate Names</div>
<div class="msg">No Spamming</div>
<div class="msg">No Advertising</div>
<div class="msg">No Excessive Insults</div>
<div class="msg">Be Respectful</div>
<div class="msg">No Swapping</div>
<div class="msg">No Heavily Spiking</div>
<div class="msg">No Bug & Glitch Abusing</div>
</div>
<div id="navigation">
 
<div id="left"></div>
<ul>
<li>
<a href="index.php">HOME</a>
<div class="hover"></div>
</li>
<li>
<a href="download.html">DOWNLOAD</a>
<div class="hover"></div>
</li>
<li class="active">
<a href="register.php">REGISTER</a>
<div class="hover"></div>
</li>
<li id="logo">
<a href="#" class="tubular-pause"></a>
<div class="hover"></div>
</li>
<li>
<a href="http://forum.titansdn.com/">FORUM</a>
<div class="hover"></div>
</li>
<li>
<a href="https://www.facebook.com/TitansDN.ID" target="_blank">FANSPAGE</a>
<div class="hover"></div>
<ul>
<li>
<a href="https://www.titansdn.com/TitansDN.ID">FANSPAGE</a>
<div class="hover"></div>
</li>
<li>
<a href="#">GRUP</a>
<div class="hover"></div>
</li>
</ul>
</li>
<li>
<a href="ranking/individual.html">RANKINGS</a>
<div class="hover"></div>
</li>
</ul>
<div id="right"></div>
</div>
<div id="wrapper">
<div id="l-sidebar">
<div class="btn status">
<div class="btn-desc">
<div class="title">
Server Status:&nbsp;<span style="color:#96ac19; font-weight:bold;">Online</span> </div>
<div class="desc"><span style="color:#ab2d2d; font-weight:bold;">35</span>&nbsp;players online</div>
</div>
</div>
<div class="side-panel">
<div class="title">INDIVIDUAL RANKING</div>
<div class="content">
<table width="200" border="0">
<tr>
<th scope="row">#</th>
<td>Player Name</td>
<td>Lv.</td>
</tr>
<tr>
<th scope="row">
<div class="gold"></div> </th>
<td><a href="#">None</a></td>
<td>0</td>
</tr>
<tr>
<th scope="row">
<div class="silver"></div> </th>
<td><a href="#">None</a></td>
<td>0</td>
</tr>
<tr>
<th scope="row">
<div class="bronze"></div> </th>
<td><a href="#">None</a></td>
<td>0</td>
</tr>
<tr>
<th scope="row">
4 </th>
<td><a href="#">None</a></td>
<td>0</td>
</tr>
<tr>
<th scope="row">
5 </th>
<td><a href="#">None</a></td>
<td>0</td>
</tr>
</table>
</div>
<div class="view-more">
<a href="#"></a>
<div class="hover"></div>
</div>
</div>
<div class="side-panel">
<div class="title">PVP RANKING</div>
<div class="content">
<table width="200" border="0">
<tr>
<th scope="row">#</th>
<td>Player Name</td>
<td>Rank.</td>
</tr>
<tr>
<th scope="row">
<div class="gold"></div> </th>
<td><a href="#">None</a></td>
<td>0</td>
</tr>
<tr>
<th scope="row">
<div class="silver"></div> </th>
<td><a href="#">None</a></td>
<td>0</td>
</tr>
<tr>
<th scope="row">
<div class="bronze"></div> </th>
<td><a href="#">None</a></td>
<td>0</td>
</tr>
<tr>
<th scope="row">
4 </th>
<td><a href="#">None</a></td>
<td>0</td>
</tr>
<tr>
<th scope="row">
5 </th>
<td><a href="#">None</a></td>
<td>0</td>
</tr>
</table>
</div>
<div class="view-more">
<a href="#"></a>
<div class="hover"></div>
</div>
</div>
<div class="side-panel">
<div class="title">OUR FACEBOOK</div>
<div class="content">
<iframe src="https://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FTitansDN.ID&amp;width=211&amp;colorscheme=dark&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:211px; height:212px;" allowTransparency="true"></iframe>
</div>
<div class="end"></div>
</div>
 
</div>
<div id="mid-container">
<div id="usercp">
<div class="panel">
<div class="content">
<div class="title">LOGIN TO YOUR TITANSDN ACCOUNT | <a href="#" class="type1">Forgot your password?</a></div>
<?php include('login.php'); ?>
<form method="post" name="login">
<div class="input type2 type3">
<input name="register" type="submit" value="REGISTER" class="button" id="register"/>
<div class="hover"></div>
</div>
</form>
</div>
</div>
</div>
<div class="news">
<div class="title">REGISTER</div>
<div class="content">
<div class="news-wrap show">
<div class="header">
<div class="active"></div>
<div class="news-title" style="text-align:center; width:459px;">SIGN UP FOR A <font color="bd2121">FREE</font> TITANSDN ACCOUNT</div>
</div>
<div class="news-container">
<form action="" method="post" id="register-form" novalidate>
<div class="register-body">Username<br/>
<input name="t_M6V4SVaH" type="hidden" value="<? echo genToken(); ?>"/>
<input name="regusername" type="text" class="type4"/>
<br/>Password<br/>
<input name="regpassword" id="regpassword" type="password" class="type4"/>
<br/>Confirm Password<br/>
<input name="regcpassword" type="password" class="type4"/>
<br/>E-Mail Address<br/>
<input name="regemail" type="text" class="type4"/>
<br />
<td valign="top">
<div class="input type2 type4" style="display: inline-block">
<input name="submit" type="submit" value="SUBMIT" class="button" id="submit"/>
<div class="hover"></div>
</div>
<div class="input type2 type3 type4" style="display: inline-block">
<input name="reset" type="reset" value="RESET ALL" class="button" id="reset"/>
<div class="hover"></div>
</div>
</td>
</tr>
</table>
<center>
<?php
  if(isset($error))
  {
    echo $error;
  }else if(isset($success))
  {
    echo $success;
  }
?>
</center>

</div>
</form>
<div class="news-end"></div>
</div>
</div>
 
</div>
<div class="end"></div>
</div>
<div id="footer">Copyright <a href="index.html">TitansDN</a> 2015 | All rights reserved. <br/>Designed by <a href="http://www.titansdn.com" title="Lead Designer">TitansDN</a></div>
</div>
 
<div id="r-sidebar">
<div class="btn download"><a href="download.html"></a>
<div class="hover"></div>
</div>
<div class="btn donate"><a href="donate.php"></a><div class="hover"></div></div>
<div class="side-panel">
<div class="title">TitansDN TRAILER</div>
<div class="content">
 <div id="trailer">
            <iframe width="180" height="140" src="https://www.youtube.com/embed/tZ_fLPUQ0bA" frameborder="0" allowfullscreen=allowfullscreen></iframe>
        </div>
</div>
<div class="end"></div>
</div>
<div class="side-panel">
	<div class="title">STATISTICS</div>
    <div class="content"><div class="body">
<span>Most Online Today: <a href="#" class="link">1337</a></span><hr />
<span>Addicted Gamer: <a href="#" class="grades grade1 tooltipSource2" data-title="Head Gamemaster">GameMaster</a></span><hr />    
<span>Last Lv. Up: <a href="#" class="grades grade5 tooltipSource2" data-title="Staff">GameMaster<font color="#d8d8d8">&nbsp;[</font>99(1)<font color="#d8d8d8">]</font></a></span><hr/>
<span>Last Rebirth: <a href="#" class="grades grade3 tooltipSource2" data-title="Developer">GameMaster<font color="#d8d8d8">&nbsp;[</font>99(99)<font color="#d8d8d8">]</font></a></span><hr/>
<span>Newest Character: <a href="#" class="link">GameMaster</a></span><hr />
Last PVP War: <br class="type1" /><a href="#" class="link clan winner" title="Won">GameMaster</a>&nbsp;vs.&nbsp;<a href="#" class="link clan" title="Lost">Players</a>
    </div></div>
	<div class="end"></div>
</div>

</div>
<script src="../ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
 
 
<script src="themes/v7/js/script.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
jQuery.validator.addMethod("require_from_group", function(value, element, options) {
  var numberRequired = options[0];
  var selector = options[1];
  var fields = $(selector, element.form);
  var filled_fields = fields.filter(function() {
    return $(this).val() != ""; 
  });
  var empty_fields = fields.not(filled_fields);
  if (filled_fields.length < numberRequired && empty_fields[0] == element) {
    return false;
  }
  return true;
// {0} below is the 0th item in the options field
}, jQuery.format("These fields are required."));
	
			//form validation rules
            $("#register-form").validate({
                rules: {
					regname: "required",
                    birthday_year: {
						require_from_group: [3, ".birthdate"]					
					},
                    birthday_month: {
						require_from_group: [3, ".birthdate"]					
					},		
                    birthday_date: {
						require_from_group: [3, ".birthdate"]					
					},												
                    regusername: "required",
                    regpassword: "required",
                    regcpassword: {
						required: true,
						equalTo: "#regpassword"
					},					
                    regemail: {
                        required: true,
                        email: true
                    },								
					regcaptcha: "required",		
                },
				groups: {
					birthdate: "birthday_year birthday_month birthday_date",
					regsecurity: "regsecurityans regsecurityq"					
				},
				errorPlacement: function (error, element) {
					if (element.attr('name') == 'regsecurityans' || element.attr('name') == 'regsecurityq') {
						error.insertAfter('#regsecurityerror');
					} else {
						error.insertAfter(element);					
					}
				}	
            });
});
</script>
<script src="themes/v7/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript">
function msg() {
  $("#banner2 div.msg:hidden:first").fadeIn(500).delay(1500).fadeOut(500, function() {
      $(this).appendTo($(this).parent());
    msg();
  });
}
msg();
</script>
<script src="themes/v7/js/trailer.js" type="text/javascript"></script>
<script src="themes/v7/nivo-slider/jquery.nivo.slider.pack.js" type="text/javascript"></script>
<script src="themes/v7/js/cufon-yui.js" type="text/javascript"></script>
<script src="themes/v7/js/Myriad_Pro_Semibold_600.font.js" type="text/javascript"></script>
<script src="themes/v7/js/jquery.infinite-carousel.js" type="text/javascript"></script>
<script src="themes/v7/js/jquery.ambiance.js" type="text/javascript"></script>
</body>
</html>