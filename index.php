﻿<? include("config.php");

if(isset($_GET['login']))
{
    echo "<script>alert('Please login to get access');</script>";
} 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TitansDN - Private Server</title>
<link href="images/favicon.gif" rel="shortcut icon" />
<link href="images/favicon.gif" rel="icon" type="image/gif"/>
<link href="style1.css" type="text/css" rel="stylesheet" />
<link href="grades.css" type="text/css" rel="stylesheet" />
<link href="nivo-slider/theme.css" type="text/css" media="screen" rel="stylesheet" />

</head>

<body>
<div id="trailer-overlay"><div id="trailer-cont"><div id="trailer-yt"></div><span id="trailer-share"><a href="#" target="_blank" class="facebook" title="Share on Facebook"></a><a href="#" target="_blank" class="twitter" title="Tweet it out!"></a></span></div></div>

<div id="navigation">
	<div id="left"></div>
        <ul>
        
            <li class="active"><a href="#">HOME</a><div class="hover"></div></li>
            <li><a href="download.html">DOWNLOAD</a><div class="hover"></div></li>
            <li><a href="register.php">REGISTER</a><div class="hover"></div></li>
            <li id="logo"><a href="main.php"></a><div class="hover"></div></li>
            <li><a href="http://forum.titansdn.com/">FORUM</a><div class="hover"></div></li>
            
            <li><a href="https://www.facebook.com/TitansDN.ID" target="_blank">FANSPAGE</a><div class="hover"></div>
            </li>    
            
            <li><a href="ranking/individual.html">RANKINGS</a><div class="hover"></div>
            </li>
            
        </ul>
	<div id="right"></div>
</div>

<div id="wrapper">

<div id="l-sidebar">
    
<div class="btn status">
	<div class="btn-desc">
		<div class="title">:&nbsp;<span style="color:#96ac19; font-weight:bold;"></span><!-- <span style="color="#ff3000; font-weight:bold;">Offline</span> --></div>
		<div class="desc"><span style="color:#ab2d2d; font-weight:bold;"></span>&nbsp;</div>
	</div>
</div>

<div class="side-panel">
	<div class="title">INDIVIDUAL RANKING</div>
    <div class="content">
  
<table width="200" border="0">
  <tr>
    <th scope="row">#</th>
    <td>Player Name</td>
    <td>Lv.</td>
  </tr>
  <tr>
    <th scope="row"><div class="gold"></div></th>
    <td><a href="#">None</a></td>  
    <td>0</td>
  </tr>
  <tr>
    <th scope="row"><div class="silver"></div></th>
    <td><a href="#">None</a></td>
    <td>0</td>
  </tr>
  <tr>
    <th scope="row"><div class="bronze"></div></th>
    <td><a href="#">None</a></td>
    <td>0</td>
  </tr>
  <tr>
    <th scope="row">4</th>
    <td><a href="#">None</a></td>
    <td>0</td>
  </tr>
  <tr>
    <th scope="row">5</th>
    <td><a href="#">None</a></td>
    <td>0</td>
  </tr>
</table>

 
    </div>
    <div class="view-more"><a href="#"></a><div class="hover"></div></div>
</div>

<div class="side-panel">
	<div class="title">PVP RANKING</div>
    <div class="content">

<table width="200" border="0">
  <tr>
    <th scope="row">#</th>
    <td>Player Name</td>
    <td>Rank.</td>
  </tr>
  <tr>
    <th scope="row"><div class="gold"></div></th>
    <td><a href="#">None</a></td>  
    <td>0</td>
  </tr>
  <tr>
    <th scope="row"><div class="silver"></div></th>
    <td><a href="#">None</a></td>
    <td>0</td>
  </tr>
  <tr>
    <th scope="row"><div class="bronze"></div></th>
    <td><a href="#">None</a></td>
    <td>0</td>
  </tr>
  <tr>
    <th scope="row">4</th>
    <td><a href="#">None</a></td>
    <td>0</td>
  </tr>
  <tr>
    <th scope="row">5</th>
    <td><a href="#">None</a></td>
    <td>0</td>
  </tr>
</table>
    
    </div>
    <div class="view-more"><a href="#"></a><div class="hover"></div></div>
</div>
        
<div class="side-panel">
	<div class="title">OUR FACEBOOK</div>
    <div class="content">
<iframe src="https://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FTitansDN.ID&amp;width=211&amp;colorscheme=dark&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:211px; height:212px;" allowTransparency="true"></iframe>
    </div>
    <div class="end"></div>
</div>

<!-- END -->        
    </div>
    
<div id="mid-container">
	<div id="usercp">
        <div class="panel">
        	<div class="content">
                <div class="title">LOGIN TO YOUR TITANSDN ACCOUNT | <a href="#" class="type1">Forgot your password?</a></div>
               <?php include('login.php'); ?>
            </div>
        </div>
    </div>
    
<!-- Slider -->
<div id="slider-frame">
	<div id="slider">     
<a href="#1"><img src="images/slides/1.png" alt="" title="<strong>NEW TITANSDN!</strong><p>TitansDN Private Server </p>" /></a>
<a href="#2"><img src="images/slides/2.png" alt="" title="<strong>NEW ITEMS</strong><p>New items have been added to all shops!</p>" /></a>
<a href="#3"><img src="images/slides/3.png" alt="" title="<strong>NEW JOB</strong><p>New Job has released!<br />New Inferface, skills & much more.</p>" /></a>
<a href="#4"><img src="images/slides/4.png" alt="" title="<strong>POWERFULL COSTUMES</strong><p>Brand-new items for all jobs<br />Click here to shop now!</p>" /></a>
	</div>
</div>
<!-- End of Slider -->



<div class="news">
	<div class="title">RECENT NEWS</div>
    <div class="content">
		<div class="news-wrap">
        	<div class="header on"><div class="active"></div>                
            	<div class="news-title">New TitansDN</div>
                <div class="date">01.06.15</div>
            </div>
            
<div class="news-container">
	<div class="body">
    
Welcome to TitansDN Players, <br/> <br/>

TitansDN Open Beta Test At 04 April 2015<br/>
SERVER RATE<br/>
* Cap 80<br/>
* Skill Ex80<br/>
* Nest Lv80<br/>
* Vulcano Nest Lv80<br/>
* Full Drop<br/>
* Easy/Fast Leveling<br/>
* Enchance Equip Max +30<br/>
* New Epic Weapon Vulcano<br/>
<br/>
Let's Join now with TitansDN. <br/>
Visit now : www.titansdn.com<br/>
<br/> 
Thanks,  <br/>
Administrator TitansDN <br />
<hr />
<span style="font-size:10px;">Added by <a href="#" class="type2">TitansDN</a></span>
	</div>   
    <div class="news-end"></div>
</div>   
        </div>
        
        
		<div class="news-wrap">
        	<div class="header on"><div class="active"></div>                 
            	<div class="news-title">TitansDN Open Beta</div>
                <div class="date">04.04.15</div>
            </div>
            
<div class="news-container">
	<div class="body">
    
TitansDN Release Open Beta Test ~
<hr />
<span style="font-size:10px;">Added by <a href="#" class="type2">TitansDN</a></span>
	</div>   
    <div class="news-end"></div>
</div>   
        </div>
        
        <div class="news-wrap">
        	<div class="header on"><div class="active"></div>                 
            	<div class="news-title">Charge T-Cash Maintenance</div>
                <div class="date">04.04.15</div>
            </div>
            
<div class="news-container">
	<div class="body">
Halo Players <br /><br />

Mohon maaf untuk sementara pengisian maupun pembelian T-Cash Shop masih di tutup, di karenakan masih dalam tahap perbaikan.<br/>
Terima Kasih.<br/><br/>

Kind regards, <br />
TitansDN Staff<br />
<hr />
<span style="font-size:10px;">Added by <a href="#" class="type2">TitansDN</a></span>
	</div>   
    <div class="news-end"></div>
</div>   
        </div>
<!-- End of Content -->     
    </div>
    <div class="end"></div>
</div>



<div class="news">
	<div id="ads-spot">
    <div id="ads-alert">Advertisement</div>
	<script type="text/javascript"><!--
        google_ad_client = "ca-pub-0562862423449276";
        /* TITANSDN */
        google_ad_slot = "4272948641";
        google_ad_width = 468;
        google_ad_height = 60;
        //-->
    </script>
    <script type="text/javascript" src="../pagead2.googlesyndication.com/pagead/show_ads.js"></script>
    </div>
	<div class="title">IKLAN</div>
    <div class="content" style="padding-bottom: 20px;">
    </div>
    <div class="end"></div>
</div>

<div id="footer">Copyright <a href="https://www.titansdn.com/">TitansDN</a> 2015 | All rights reserved. <br />Designed by <a href="http://www.titansdn.com/" title="Lead Designer">TitansDN</a></div>

</div>
<!-- Middle Container -->

<div id="r-sidebar">

<div class="btn download"><a href="download.html"></a><div class="hover"></div></div>


<div class="side-panel">
	<div class="title">TitanDN TRAILER</div>
    <div class="content">
        <div id="trailer">
            <iframe width="180" height="140" src="https://www.youtube.com/embed/tZ_fLPUQ0bA" frameborder="0" allowfullscreen=allowfullscreen></iframe>
        </div>
    </div>
	<div class="end"></div>
</div>

<div class="side-panel">
	<div class="title">STATISTICS</div>
    <div class="content"><div class="body">
<span>Most Online Today: <a href="#" class="link">1337</a></span><hr />
<span>Addicted Gamer: <a href="#" class="grades grade1 tooltipSource2" data-title="Head Gamemaster">GameMaster</a></span><hr />    
<span>Last Lv. Up: <a href="#" class="grades grade5 tooltipSource2" data-title="Staff">GameMaster<font color="#d8d8d8">&nbsp;[</font>80(1)<font color="#d8d8d8">]</font></a></span><hr/>
<span>Newest Character: <a href="#" class="link">GameMaster</a></span><hr />

    </div></div>
	<div class="end"></div>
</div>

<div class="btn donate"><a href="donate.php"></a><div class="hover"></div></div>


</div>


</div>


<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>
<script src="js/trailer.js" type="text/javascript"></script>
<script src="nivo-slider/jquery.nivo.slider.pack.js" type="text/javascript"></script>
<script src="js/cufon-yui.js" type="text/javascript"></script>
<script src="js/Myriad_Pro_Semibold_600.font.js" type="text/javascript"></script>
<script src="js/jquery.infinite-carousel.js" type="text/javascript"></script>
<script>

</script>


</body>
</html>
