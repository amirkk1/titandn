<?php
/*
 * MS-SQL database class - Catalin Alin 2014
 * @version 1.6
 *
 * This class provides basic methods for working with a MS-SQL database
 *
 * usage :
 * create object from this class
 * $sqlObj = new mssqlClass();
 * 
 * make a connection
 * $sqlObj->connect($host, $user, $password);
 * 
 *  select database
 * $sqlObj->select($dbName);
 * 
 * query the db, check query success, check for results 
 * and place results in an array of objects.
 * 
 * 
 * $resultArray = false;
 * if($sqlObj->query("select * from table")){
 * 	if($sqlObj->numRows() > 0){
 * 		$resultArray = array();
 * 		while($row = $sqlObj->fetchObject(){
 * 			$resultArray[] = $row;
 * 		}
 * 	}else{
 * 		echo "No Results Found";
 * 	}
 * }else{
 * 	echo $sqlObj->getLastErrorMessage(); 
 * }
 * 
 * 
 */



class mssqlClass {
	private $result; // Query result
	private $querycount; // Total queries executed
	private $linkid = false;

	/**
	 * method to connect to the database server.
	 **/

	function connect($sqlHost, $sqlUser, $sqlPassword) {


		$this->linkid = sqlsrv_connect ( $sqlHost, array("Database"=>"dnMembership"));// $sqlUser, $sqlPassword );

		if( $this->linkid ) {
			// echo "Connection established.<br />";
		}else{
			// echo "Connection could not be established.<br />";
			 die( print_r( sqlsrv_errors(), true));
		}

	}

	
	function getLink()
	{
		return $this->linkid;
	}
	

	/** 
	 * method to select a database.
	 **/

	/*function select($sqlDatabase) {

		try {

			if (! @mssql_select_db ( $sqlDatabase, $this->linkid ))

				throw new Exception ( "Problems in fiding the database." );

		} catch ( Exception $e ) {

			die ( $e->getMessage () );

		}

	}*/

	/**
	*returns array on given query
	*/

	function query_array($query)
	{
		
			$result = array();
			$query = sqlsrv_query($this->linkid,$query);

			while ($row = sqlsrv_fetch_array($query)) {
				$result[] = $row;
			}

			return $result;
			
	}
	

	/**
	 * method to query sql database
	 * take mssql query string
	 * returns false if no results or NULL result is returned by query
	 * if query action is not expected to return results eg delete
	 * returns false on sucess else returns result set
	 * @param unknown_type $query
	 * @return unknown
	 */

	function query($query) {
	
		$this->result = sqlsrv_query ( $this->linkid ,$query ,array(), array('Scrollable' => 'buffered'));
		
		if (! $this->result) {
			return FALSE;
		} else {
			return $this->result;
		}
	}
	
	/**
	 * method to return the number of rows affected by query.
	 **/

	function affectedRows() {

		return sqlsrv_rows_affected( $this->linkid );

	}

	

	/** 
	 * method to determine the number of rows returned by query. 
	 **/

	function numRows() {

		return @sqlsrv_num_rows( $this->result );

	}

	

	/**
	 * method to return a query result row as an object. 
	 **/

	function fetchObject() {

		return @mssql_fetch_object ( $this->result );

	}

	

	/** 
	 * method to return a query result row 
	 * as an indexed array 
	 **/

	function fetchRow() {

		return @mssql_fetch_row ( $this->result );

	}

	

	/**
	 * method to return a query result row 
	 * as an associative array. 
	 **/

	function fetchArray() {

		return @mssql_fetch_array ( $this->result, MSSQL_ASSOC );

	}

	

	/**
	 * method to return the total number 
	 * queries executed during lifetime of this object.
	 *
	 * @return int
	 **/

	function numQueries() {
		return $this->querycount;
	}

	

	/**
	 * method to save a result set in to the object
	 * 
	 * @param ResultSet $resultSet
	 **/

	private function setResult($resultSet) {

		$this->result = $resultSet;

	}

	

	/**
	 * method to eturn the number of fields in a result set.
	 **/

	function numberFields() {

		return @mssql_num_fields ( $this->result );

	}

	

	/**
	 * method to return a field name given an integer offset. 
	 *
	 * @param int $offset
	 **/

	function fieldName($offset) {

		return @mssql_field_name ( $this->result, $offset );

	}

	

	/**
	 * method to return the last error message from the db sqerver
	 */

	function getLastErrorMessage() {

		return @mssql_get_last_message ();

	}

}



?>