$(function() {
	jQuery('.news-wrap > .header.off').click(function() {
		jQuery(this).toggleClass('selected').next().slideToggle();		
	});			
	jQuery('.news-wrap > .news-container').hide();
	jQuery('.news-wrap.show > .news-container').show();		
	jQuery('.header:first').addClass('selected').next().show();	
	jQuery('.header.deselect').removeClass('selected');	
	jQuery('.news-wrap > .header.on').click(function() {
		if (jQuery(this).next().is(':hidden')) {
			jQuery('.news-wrap > .header.on').removeClass('selected').next().slideUp();
			jQuery(this).addClass('selected').next().slideDown();
		}
	});
	
	var z = window.location.hash;
	if (z != '') {
		$('.news-wrap' + z +  ' .header').addClass('selected').next().show();
	};
	
	$('.news-wrap').click(function(event) {
		if (($(this).attr('id')) != null) {
        	top.location.hash = '#' + $(this).attr('id');
		}
    });
			
 	$('#slider').nivoSlider({
		pauseTime: 4000,
		directionNav: false  
	});
			
	setInterval(function() {
		var element = $('.nivo-caption').height();
		var test = '-' + (element / 2) + 'px';
		$('.nivo-caption').css('margin-top',test);
	}, 1);
	
	Cufon.replace('.side-panel .title, .news > .title, .btn-desc.type2 .title', { fontFamily: 'Myriad Pro Semibold', hover: true }); 
	function handleError() { return true; }
	window.onerror = handleError;
	
    $('.side-panel .content > table tr').click ( function() {
        window.location = $(this).find('a').attr('href');
    });

    $('table.rankinglist.toggled tr').click ( function() {
        window.location = $(this).find('a.redirect').attr('href');
    });
		
	$('.side-panel .content > table tr:nth-child(1n+2)').hover ( function() {
        $(this).stop().animate({opacity:1});
    }, function() {
		$(this).stop().animate({opacity:0.7});
	});
	
	//item list slider
	var $container = $("#viewport-cont ul");
		$container.html(shuffle($container.children().get()));
		
		function shuffle(o){
			for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
			return o;
	};

	$('#viewport-cont').carousel('#simplePrevious', '#simpleNext','#auto');
	
	var hovered = false;
		
	function slide(){
		if (!hovered) {
		  $('#auto').click();
		}
	}
	
	var intervalId = window.setInterval(slide, 6000);
	
	$('#viewport-cont, #simplePrevious, #simpleNext').hover ( function(event) {
	  if(event.originalEvent){
		   hovered = true;
	  }
	}, function() {
		hovered = false;
	});
	//end of item list slider
	
	// Tooltip
	$('.tooltipSource').hover(function(){
			$('body').append('<div class="tooltip"><table border="0" cellpadding="0" class="tooltiptable"><tr><th width="100" colspan="2" style="text-align: center; background-color:  RGBA(26,7,7,0.2); color: #e2e2e0;">'+$(this).attr('data-category')+'</th></tr><tr><th>TYPE</th><td>'+$(this).attr('data-type')+'</td></tr><tr><th>SEX</th><td>'+$(this).attr('data-sex')+'</td></tr><tr><th>DAMAGE</th><td>'+$(this).attr('data-dmg')+'</td></tr><tr><th>DELAY</th><td>'+$(this).attr('data-delay')+'</td></tr><tr class="range"><th>RANGE</th><td>'+$(this).attr('data-rg')+'</td></tr><tr><th>MAGAZINE</th><td>'+$(this).attr('data-mg')+'</td></tr><tr><th>WEIGHT</th><td>'+$(this).attr('data-wg')+'</td></tr><tr><th>HP</th><td>'+$(this).attr('data-hp')+'</td></tr><tr><th>AP</th><td>'+$(this).attr('data-ap')+'</td></tr><tr><th>RELOAD TIME</th><td>'+$(this).attr('data-rt')+'</td></tr><tr><th colspan="2" style="text-align: center;">'+$(this).attr('data-extra')+'</th></tr></table></div>');		
			$('.tooltip').fadeIn();	
			
			if ($(this).attr('data-sex') == 0 ) {
				$('.tooltiptable tr:nth-child(3)').css('display','none');
			}
			if ($(this).attr('data-dmg') == 0 ) {
				$('.tooltiptable tr:nth-child(4)').css('display','none');
			}
			if ($(this).attr('data-delay') == 0 ) {
				$('.tooltiptable tr:nth-child(5)').css('display','none');
			}			
			if ($(this).attr('data-rg') == 0 ) {
				$('.tooltiptable tr:nth-child(6)').css('display','none');
			} 
			if ($(this).attr('data-mg') == 0 ) {
				$('.tooltiptable tr:nth-child(7)').css('display','none');
			}
			if ($(this).attr('data-wg') == 0 ) {
				$('.tooltiptable tr:nth-child(8)').css('display','none');
			}	
			if ($(this).attr('data-hp') == 0 ) {
				$('.tooltiptable tr:nth-child(9)').css('display','none');
			}
			if ($(this).attr('data-ap') == 0 ) {
				$('.tooltiptable tr:nth-child(10)').css('display','none');
			}								
			if ($(this).attr('data-rt') == 0 ) {
				$('.tooltiptable tr:nth-child(11)').css('display','none');
			}
			if ($(this).attr('data-extra') == 0 ) {
				$('.tooltiptable tr:nth-child(12)').css('display','none');
			}			
			
	}, function() {
			$('.tooltip').remove();
	
	}).mousemove(function(e) {
			var mousex = e.pageX + 20; //Get X coordinates
			var mousey = e.pageY + 10; //Get Y coordinates
			$('.tooltip').css({ top: mousey, left: mousex })
	});
	
	//Tooltip 2
	$('.tooltipSource2').hover(function(){
			$('body').append('<div class="tooltip">'+$(this).attr('data-title')+'</div>');		
			$('.tooltip').fadeIn();						
	}, function() {
			$('.tooltip').remove();
	
	}).mousemove(function(e) {
			var mousex = e.pageX + 20; //Get X coordinates
			var mousey = e.pageY + 10; //Get Y coordinates
			$('.tooltip').css({ top: mousey, left: mousex })
	});	

	$('#navigation ul li').hover( function() {
		$(this).find('ul').stop(true, true).slideDown(200);
	}, function() {
		$(this).find('ul').stop(true, true).fadeOut();	
	});
	
	if ( ($('.clan').text().length) > 15) {
		$('br.type1').css('display','block')
	}
	
	$('#download-area1, #download-area2').click(function() {
		$(this).find('.active').stop(true, true).show();
	});
	
	$('#download-area1, #download-area2').mouseout(function() {
		$(this).find('.active').stop(true, true).hide();
	});	
	
	$('.clan-body .emblem .uploadem').click(function() {
		$('.clan-body #uploadembox').fadeIn();	
		$('.clan-body #clandisbandbox').fadeOut();			
	});

	$('.clan-body #uploadembox #exit').click(function() {
		$('.clan-body #uploadembox').fadeOut();	
	});
		
	$('.clan-body #disbandclanbutton').click(function() {
		$('.clan-body #clandisbandbox').fadeIn();	
		$('.clan-body #uploadembox').fadeOut();			
	});

	$('.clan-body #clandisbandbox #exit').click(function() {
		$('.clan-body #clandisbandbox').fadeOut();
	});
		
	$('#colorstorage #namelist').keyup(function () { 
		var g = $(this).val();
		if (g == '') {
			$('input#namelist').val('Preview');
			$('label').text('Preview');
		} else {
			$('label').text(g);   
		}
	});
	
	setInterval(function() {
		$('.ugrade2').toggleClass('ugrade2v')
	}, 1000);

	$('#usercp input').mousedown(function() {
		$('.error.auto').fadeOut(1000);
	})
});
