﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="reg.vb" Inherits="register" %>
<%@ Register Assembly="BotDetect" Namespace="BotDetect.Web.UI" TagPrefix="BotDetect" %>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
                      <head runat="server">
                          <meta charset="utf-8" />
                          <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
                          <title>TitansDN - Register</title>
                          <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                          <title>TitansDN - Private Server</title>
                          <link href="images/favicon.gif" rel="shortcut icon" />
                          <link href="images/favicon.gif" rel="icon" type="image/gif" />
                          <link href="style1.css" type="text/css" rel="stylesheet" />
                          <link href="grades.css" type="text/css" rel="stylesheet" />
                          <link href="nivo-slider/theme.css" type="text/css" media="screen" rel="stylesheet" />
                          <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
                      </head>

<body>
    <div id="trailer-overlay"><div id="trailer-cont"><div id="trailer-yt"></div><span id="trailer-share"><a href="#" target="_blank" class="facebook" title="Share on Facebook"></a><a href="#" target="_blank" class="twitter" title="Tweet it out!"></a></span></div></div>

    <div id="navigation">
        <div id="left"></div>
        <ul>

            <li class="active"><a href="index.html">HOME</a><div class="hover"></div></li>
            <li><a href="download.html">DOWNLOAD</a><div class="hover"></div></li>
            <li><a href="default.aspx">REGISTER</a><div class="hover"></div></li>
            <li id="logo"><a href="#"></a><div class="hover"></div></li>
            <li><a href="#">FORUM</a><div class="hover"></div></li>

            <li>
                <a href="https://www.facebook.com/TitansDN.ID" target="_blank">FANSPAGE</a><div class="hover"></div>
            </li>

            <li>
                <a href="ranking.html">RANKINGS</a><div class="hover"></div>
                <ul>
                    <li><a href="#">Individual</a><div class="hover"></div></li>
                    <li><a href="#">PVP</a><div class="hover"></div></li>
                </ul>
            </li>

        </ul>
        <div id="right"></div>
    </div>

    <div id="wrapper">

        <div id="l-sidebar">

            <div class="btn status">
                <div class="btn-desc">
                    <div class="title">Server Status:&nbsp;<span style="color:#96ac19; font-weight:bold;">Online</span><!-- <span style="color="#ff3000; font-weight:bold;">Offline</span> --></div>
                    <div class="desc"><span style="color:#ab2d2d; font-weight:bold;">125/500</span>&nbsp;players</div>
                </div>
            </div>

            <div class="side-panel">
                <div class="title">INDIVIDUAL RANKING</div>
                <div class="content">

                    <table width="200" border="0">
                        <tr>
                            <th scope="row">#</th>
                            <td>Player Name</td>
                            <td>Lv.</td>
                        </tr>
                        <tr>
                            <th scope="row"><div class="gold"></div></th>
                            <td><a href="#">None</a></td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <th scope="row"><div class="silver"></div></th>
                            <td><a href="#">None</a></td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <th scope="row"><div class="bronze"></div></th>
                            <td><a href="#">None</a></td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <th scope="row">4</th>
                            <td><a href="#">None</a></td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <th scope="row">5</th>
                            <td><a href="#">None</a></td>
                            <td>0</td>
                        </tr>
                    </table>


                </div>
                <div class="view-more"><a href="#"></a><div class="hover"></div></div>
            </div>

            <div class="side-panel">
                <div class="title">PVP RANKING</div>
                <div class="content">

                    <table width="200" border="0">
                        <tr>
                            <th scope="row">#</th>
                            <td>Player Name</td>
                            <td>Rank.</td>
                        </tr>
                        <tr>
                            <th scope="row"><div class="gold"></div></th>
                            <td><a href="#">None</a></td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <th scope="row"><div class="silver"></div></th>
                            <td><a href="#">None</a></td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <th scope="row"><div class="bronze"></div></th>
                            <td><a href="#">None</a></td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <th scope="row">4</th>
                            <td><a href="#">None</a></td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <th scope="row">5</th>
                            <td><a href="#">None</a></td>
                            <td>0</td>
                        </tr>
                    </table>

                </div>
                <div class="view-more"><a href="#"></a><div class="hover"></div></div>
            </div>

            <div class="side-panel">
                <div class="title">OUR FACEBOOK</div>
                <div class="content">
                    <iframe src="https://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FTitansDN.ID&amp;width=211&amp;colorscheme=dark&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:211px; height:212px;" allowtransparency="true"></iframe>
                </div>
                <div class="end"></div>
            </div>

            <!-- END -->
        </div>

        <div id="mid-container">
            <div id="usercp">
                <div class="panel">
                    <div class="content">

                    </div>
                </div>
            </div>


            <div class="news">
                <div class="title">REGISTER</div>
                <div class="content">
                    <div class="news-wrap">
                        <div class="header on">
                            <div class="active"></div>
                            <div class="news-title">SIGN UP FOR <font color="red">FREE</font> TITANSDN ACCOUNT</div>

                        </div>

                        <div class="news-container">
                            <div class="body">

                                <form id="form1" runat="server">
                                    <div class="container">
                                        <section class="register">
                                            <h1>Register TitansDN</h1>
                                            <div class="reg_section personal_info">
                                                <table class="style1">
                                                    <tr>
                                                        <td class="style2" align=left>
                                                            <asp:Label ID="Label2" runat="server" Text="Username:"></asp:Label>
                                                        </td>
                                                        <td class="style3">
                                                            <asp:TextBox ID="TextBox1" runat="server" Width="169px"></asp:TextBox><br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style2" align=left>
                                                            <asp:Label ID="Label6" runat="server" Text="Email:"></asp:Label>
                                                        </td>
                                                        <td class="style3">
                                                            <asp:TextBox ID="TextBox6" runat="server" Width="169px"></asp:TextBox>
                                                        </td>
                                                    </tr>

                                                </table>

                                            </div>
                                            <div class="reg_section password">

                                                <tr>
                                                    <td class="style2" align=left>
                                                        <asp:Label ID="Label3" runat="server" Text="Password:"></asp:Label>
                                                    </td>
                                                    <td class="style3">
                                                        <asp:TextBox ID="TextBox2" runat="server" Width="169px" TextMode="Password"></asp:TextBox><br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style2" align=left>
                                                        <asp:Label ID="Label4" runat="server" Text="Confirm:"></asp:Label>
                                                    </td>
                                                    <td class="style3">
                                                        <asp:TextBox ID="TextBox3" runat="server" Width="169px" TextMode="Password"></asp:TextBox>
                                                    </td>
                                                </tr>

                                            </div>
                                            <div class="reg_section password">
                                                <tr>
                                                    <td class="style2" align=left>
                                                        <asp:Label ID="Label5" runat="server" Text="Hint:"></asp:Label>
                                                    </td>
                                                    <td class="style3">
                                                        <asp:TextBox ID="TextBox5" runat="server" Width="169px"></asp:TextBox>
                                                    </td>
                                                </tr>

                                                <div class="reg_section password">
                                                    <tr>
                                                        <td class="style2" align=left>
                                                            <asp:Label ID="Label7" runat="server" Text="Captcha"></asp:Label>
                                                        </td>

                                                        <td align=left>
                                                            <BotDetect:Captcha ID="SampleCaptcha" runat="server" />
                                                        </td>

                                                        <td class="style3" align=left>

                                                            <asp:TextBox ID="CaptchaCodeTextBox" runat="server" Width="169px"></asp:TextBox>
                                                        </td>

                                                    </tr>
                                                </div>
                                                <tr>
                                                    <td class="style3">
                                                        &nbsp;
                                                    </td>

                                                    <td>

                                                        <p class="submit">
                                                            <asp:Button ID="Submit" runat="server" Text="Daftar !" Width="174px" />
                                                        </p>
                                                    </td>
                                                </tr>
                                                </table>
                                                <center>
                                                    <asp:Label ID="Label1" runat="server" Text="" ForeColor=Red></asp:Label>
                                                </center>

                                            </div>
                                        </section>
                                    </div>
                                </form>
                            </div>
                        </div>
                        </form>
                        <hr />
                        <span style="font-size:10px;">Jaga baik-baik data TitansDN Anda. Regard's <a href="http://titansdn.com" class="type2">TitansDN</a></span>
                    </div>
                    <div class="news-end"></div>
                </div>

            </div>
            <!-- End of Content -->
        </div>
        <div class="end"></div>
    </div>



    <div class="news">
        <div id="ads-spot">
            <div id="ads-alert">Advertisement</div>
            <script type="text/javascript">
<!--
        google_ad_client = "ca-pub-0562862423449276";
        /* TITANSDN */
        google_ad_slot = "4272948641";
        google_ad_width = 468;
        google_ad_height = 60;
        //-->
            </script>
            <script type="text/javascript" src="../pagead2.googlesyndication.com/pagead/show_ads.js"></script>
        </div>
        <div class="title">IKLAN</div>
        <div class="content" style="padding-bottom: 20px;">
        </div>
        <div class="end"></div>
    </div>

    <div id="footer">Copyright <a href="https://www.titansdn.com/">TitansDN</a> 2015 | All rights reserved. <br />Designed by <a href="http://www.titansdn.com/" title="Lead Designer">TitansDN</a></div>

    </div>
    <!-- Middle Container -->

    <div id="r-sidebar">

        <div class="btn download"><a href="http://www.titansdn.com/client/TitansDN.rar"></a><div class="hover"></div></div>


        <div class="side-panel">
            <div class="title">TITANSDN TRAILER</div>
            <div class="content">
                <div id="trailer">
                    <iframe width="180" height="140" src="https://www.youtube.com/embed/tZ_fLPUQ0bA" frameborder="0" allowfullscreen=allowfullscreen></iframe>
                </div>
            </div>
            <div class="end"></div>
        </div>

        <div class="side-panel">
            <div class="title">STATISTICS</div>
            <div class="content">
                <div class="body">
                    <span>Most Online Today: <a href="#" class="link">1337</a></span><hr />
                    <span>Addicted Gamer: <a href="#" class="grades grade1 tooltipSource2" data-title="Head Gamemaster">GameMaster</a></span><hr />
                    <span>Last Lv. Up: <a href="#" class="grades grade5 tooltipSource2" data-title="Staff">GameMaster<font color="#d8d8d8">&nbsp;[</font>80(1)<font color="#d8d8d8">]</font></a></span><hr />
                    <span>Newest Character: <a href="#" class="link">GameMaster</a></span><hr />
                </div>
            </div>
            <div class="end"></div>
        </div>

        <div class="btn donate"><a href="#"></a><div class="hover"></div></div>



    </div>


    </div>


    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/script.js" type="text/javascript"></script>
    <script src="js/trailer.js" type="text/javascript"></script>
    <script src="nivo-slider/jquery.nivo.slider.pack.js" type="text/javascript"></script>
    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/Myriad_Pro_Semibold_600.font.js" type="text/javascript"></script>
    <script src="js/jquery.infinite-carousel.js" type="text/javascript"></script>
    <script>

    </script>

</body>
</html>
